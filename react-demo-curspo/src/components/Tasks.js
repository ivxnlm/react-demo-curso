import React from 'react'
import Tarea from './Task'
import Prototipo from 'prop-types'

class Tasks extends React.Component{

    render(){
        return(
            <div>
                <h1>Tareas</h1>
                {this.props.listTareas.map(tarea => <div key={tarea.id}>
                    <Tarea task={tarea} deleteTask={this.props.deleteTask} checkDone={this.props.checkDone}></Tarea>
                    </div>)}
            </div>
        )
    }
}

Tasks.propTypes = {
    listTareas:Prototipo.array.isRequired
}

export default Tasks