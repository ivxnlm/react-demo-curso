import React from 'react'

class TaskForm extends React.Component{

    state = {
        title:'',
        description : ''
    }

    sendData = (e) => {
        e.preventDefault();
        console.log(this.state)
        this.props.addTarea(this.state.title,this.state.description)
    }

    writeData = (e) =>{
        this.setState({[e.target.name]:e.target.value})
    }


    render(){
        return(
            <div id="tarea">
                <h3>Formulario</h3>
                <form onSubmit={this.sendData}>
                    <input name="title" type="text" placeholder="Write something" onChange={this.writeData} value={this.state.title}/>
                    <br/>
                    <textarea name="description" placeholder="Write a description" onChange={this.writeData} value ={this.state.description}></textarea>
                    <input type="submit" value="Send"/>
                </form>
            </div>
        )
    }
}

export default TaskForm