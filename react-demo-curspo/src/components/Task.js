import React from 'react'
import './Task.css'
import Prototipo from 'prop-types'

class Task extends React.Component{


    StyleCompleted(){
        return (
            {
                fontSize:'20px',
                color: this.props.task.done?'green':'red',
                textDecoration:'none'
            }
        )
    }


    render(){

        const {task} = this.props
        const pinkColor = {background:'pink'}
        return(
            <div id="tarea" style={this.StyleCompleted()}>
                <h3>Tarea {task.id}</h3>
                <h4>title: {task.title}</h4>
                <h4>Desc: {task.description}</h4>
                <h4>Done: {task.done?"true":"false"}</h4>
                <input type="checkbox" name="" id="" onChange={this.props.checkDone.bind(this,task.id)}/>
                
                <button style={btnDelete} onClick={this.props.deleteTask.bind(this,task.id)}>Eliminar</button>
                
                <div style={{background:'orange'}}>
                    <footer>Poniendo estilo con style</footer>
                </div>

                <div style={pinkColor}>
                    <footer>poniendo estilo con objeto</footer>
                </div>
            </div>
        )
    }
}

Task.propTypes = {
    task: Prototipo.object.isRequired
}

const btnDelete = {
    fontSize: '18px',
    background:'#ea2027',
    color:'#fff',
    border:'none',
    padding:'10px 15px',
    borderRadius:'50%',
    cursor:'pointer'
}

export default Task