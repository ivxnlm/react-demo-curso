import React from 'react';
import './App.css';

import tareas from './sample/tasks.json'
import Tareas from './components/Tasks'
import TaskForm from './components/TaskForm'
import Post from './fetching/Post';
import {BrowserRouter as Router, Route,Link} from 'react-router-dom'
import AddContactForm from './redux/components/AddContactForm';

class App extends React.Component{

  state = {
    tasks:tareas
  }

  addTask = (title, description) =>{
    console.log('add a task'+title+description)
    const newTask = {
      id:this.state.tasks.length+1,
      title:title,
      description:description,
      done:false
    }
    this.setState({tasks:[...this.state.tasks,newTask]})
  }

  deleteTask = (id) =>{
    const newTasks =  this.state.tasks.filter( (tarea)=> tarea.id !== id)
    this.setState({tasks:newTasks})
  }

  checkDone = (id) =>{
    const newTareas = this.state.tasks.map( (tarea)=>{
      if(tarea.id === id){
        tarea.done = !tarea.done
      }
      return tarea
    })
    this.setState({tasks:newTareas})

  }

  render(){
    return(
      <div>
        <Router>
        <Link to="/">Home</Link><br/>
        <Link to="/post">Post</Link><br/>
        <Link to="/redux">Redux</Link>

        <Route patch="/redux" component={AddContactForm}></Route>
        <Route exact path="/" render= {()=>{
          return <div>
          <TaskForm addTarea={this.addTask}></TaskForm>
        <Tareas listTareas={this.state.tasks} deleteTask={this.deleteTask} checkDone={this.checkDone}></Tareas>
          </div>
          }}>
          </Route>
          <Route path="/post" component={Post}></Route>
        </Router>
      </div>
    )
  }
}

export default App;
