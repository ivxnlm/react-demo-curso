import React, { Component } from 'react'

class Post extends Component {

    state = {
        posts:[]
    }

    //el componente ha sido montado (ha sido cargado en pantalla)
    async componentDidMount(){
        const res = await fetch('https://jsonplaceholder.typicode.com/posts')
        const data = await res.json()
        console.log(data)
        this.setState({posts:data})
    }


    render(){
        return (
            <div>
                <h1>Posts</h1>
                {this.state.posts.map( (post) => 
                            <div key={post.id}>
                                <h3>{post.id}</h3>
                                <h3>{post.title}</h3>
                                <p>{post.body}</p>
                            </div>
                )}
            </div>
        )
    }
}

export default Post
